# **HISTOIRE DONT JE SUIS LE HEROS**

# Informations générales

Ce projet est un **site web interactif** dans lequel **VOUS** pouvez faire des choix. Ces choix auront une influence sur l'histoire ainsi que sur le héros que vous incarnez.

## Auteurs

**Télio CORRE**,**Mathis DORE**,**Thomas VALENTE**, **Tom COLLIN** quatres étudiants en première année à **Campus Academy Rennes** ont participé à l'élaboration de ce projet.

## Comment faire ?

Naviguez sur notre site, incarnez votre héros et survivez !!